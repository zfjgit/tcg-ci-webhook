package com.tcg.ci.webhook.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
public class TelegramConfig {

    @Value("${telegram.https.certificate.file}")
    private String httpsCertificateFile;

    @Value("${telegram.bots.api.url}")
    private String botsApiUrl;

    @Value("${telegram.bots.api.token}")
    private String botsApiToken;

    @Value("${telegram.bots.api.setwebhook.method}")
    private String botsApiSetWebhookMethod;

    @Value("${telegram.bots.api.sendmessage.method}")
    private String botsApiSendMessageMethod;

    @Value("${telegram.bots.api.getwebhookinfo.method}")
    private String botsApiGetWebhookInfoMethod;

    @Value("${telegram.bots.webhook.url}")
    private String webhookUrl;

    @Value("${telegram.bots.webhook.update.url}")
    private String webhookUpdateUrl;

    /**
     * this parameter is not required
     *
     * @see <a href="https://core.telegram.org/bots/webhooks#how-do-i-set-a-webhook-for-either-type">How do I set a webhook for either type?</a>
     */
    @Value("${telegram.bots.webhook.certificate.file:}")
    private String webhookCertificateFile;

    @Value("${telegram.bots.webhook.instant-view-url.compare}")
    private String webhookInstantViewUrlCompare;
}
