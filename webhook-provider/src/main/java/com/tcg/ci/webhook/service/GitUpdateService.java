package com.tcg.ci.webhook.service;

import java.io.IOException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.parser.ParserConfig;
import com.google.common.collect.Sets;
import com.tcg.ci.webhook.cache.TelegramUpdateCache;
import com.tcg.ci.webhook.config.TelegramConfig;
import com.tcg.ci.webhook.data.git.Commit;
import com.tcg.ci.webhook.data.git.GitlabEvent;
import com.tcg.ci.webhook.utils.OkHttpUtils;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Slf4j
@Service
public class GitUpdateService implements IGitUpdateService {

    @Autowired
    private OkHttpUtils okHttpUtils;

    @Autowired
    private TelegramUpdateCache updateCache;

    @Autowired
    private TelegramConfig telegramConfig;

    @Override
    public void onUpdate(JSONObject updates, String s) {
        if (updates == null) {
            log.error("git updates is null");
            return;
        }

        if (StringUtils.isBlank(s)) {
            log.error("git update url(s) is null");
            return;
        }

        JSONArray chatIds = null;
        JSONObject updateChatCache = updateCache.getUpdateChatCache();
        if (updateChatCache.get(s) != null) {
            chatIds = updateChatCache.getJSONArray(s);
        }

        if (CollectionUtils.isEmpty(chatIds)) {
            log.error("chatIds is null");
            return;
        }

        final String gitUpdates = getGitUpdates(updates);
        log.info("gitUpdates={}", gitUpdates);

        if (StringUtils.isBlank(gitUpdates)) {
            log.error("gitUpdates is parse error, ignore this update.");
            return;
        }

        Sets.newHashSet(chatIds).parallelStream().forEach(cid -> {
            try {
                JSONObject params = new JSONObject();
                params.put("text", gitUpdates);
                params.put("chat_id", cid);
                params.put("parse_mode", "Markdown");

                RequestBody requestBody = RequestBody.create(params.toJSONString().getBytes("utf-8"), MediaType.get("application/json"));

                String url = telegramConfig.getBotsApiUrl() + telegramConfig.getBotsApiToken() + "/"
                        + telegramConfig.getBotsApiSendMessageMethod();
                log.info("onUpdate send git updates message, url={}", url);
                Request request = new Request.Builder().url(url).post(requestBody).build();

                @Cleanup
                Response response = okHttpUtils.httpClient().newCall(request).execute();
                log.info("git updates response:{}", JSON.toJSONString(response));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    private String getGitUpdates(JSONObject updates) {
        log.info("git updates={}", updates);

        String objectKing = updates.getString("object_kind");
        if (StringUtils.isBlank(objectKing) || !"push".equals(objectKing)) {
            return null;
        }

        ParserConfig config = new ParserConfig();
        config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;

        GitlabEvent gitlabEvent = updates.toJavaObject(GitlabEvent.class, config, 0);
        if (gitlabEvent == null) {
            return null;
        }

        String ref = gitlabEvent.getRef();
        String after = gitlabEvent.getAfter().substring(0, 10);
        String before = gitlabEvent.getBefore().substring(0, 10);
        String branch = ref.substring(ref.lastIndexOf("/") + 1);
        String branchName = gitlabEvent.getProject().getName() + "/" + branch;
        String branchUrl = gitlabEvent.getProject().getWebUrl() + "/tree/" + branch;
        String compareUrl = gitlabEvent.getProject().getWebUrl() + "/compare/" + before + "..." + after;
        StringBuilder msg = new StringBuilder();
        msg.append("*" + gitlabEvent.getUserName() + "*");

        List<Commit> commits = gitlabEvent.getCommits();
        if (CollectionUtils.isEmpty(commits)) {
            // delete or create branch
            if (StringUtils.containsOnly(before, "0")) {
                msg.append(" created branch [" + branchName + "](" + branchUrl + ")");
            } else {
                msg.append(" deleted branch *" + branchName + "*");
            }
        } else {
            long totalCommitsCount = gitlabEvent.getTotalCommitsCount();

            int added = 0;
            int removed = 0;
            int modified = 0;
            for (Commit commit : commits) {
                added += commit.getAdded().size();
                removed += commit.getRemoved().size();
                modified += commit.getModified().size();
            }

            StringBuilder changes = new StringBuilder();
            if (modified > 0) {
                changes.append(modified + " files modified ");
            }
            if (added > 0) {
                changes.append(added + " added ");
            }
            if (removed > 0) {
                changes.append(removed + " removed");
            }

            StringBuilder compareTemplateUrl = new StringBuilder(telegramConfig.getWebhookInstantViewUrlCompare());
            compareTemplateUrl.append("?beforeAfter=" + before + "..." + after);
            compareTemplateUrl.append("&changes=" + changes.toString());
            compareTemplateUrl.append("&commits=" + totalCommitsCount + " commits");
            compareTemplateUrl.append("&compareUrl=" + compareUrl);

            msg.append(" [pushed](" + compareTemplateUrl + ") to [" + branchName + "](" + branchUrl + ")");

            StringBuilder ids = new StringBuilder();
            for (Commit commit : commits) {
                String commitUrl = commit.getUrl();
                String comment = commit.getMessage();
                msg.append("\n");
                msg.append("*" + gitlabEvent.getUserUsername() + ":* [" + comment + "](" + commitUrl + ")");

                if (commits.size() <= 2) {
                    ids.append("#" + commit.getId().substring(0, 10) + " ");
                }
            }

            if (commits.size() > 2) {
                ids.append("#" + commits.get(0).getId().substring(0, 10) + " ... #"
                        + commits.get(commits.size() - 1).getId().substring(0, 10));
            }
            msg.append("\n");
            msg.append("[" + totalCommitsCount + " commits](" + compareTemplateUrl + ")\n");
            msg.append("*" + ids + "*\n");
            if (modified > 0) {
                msg.append("*" + modified + "* files modified ");
            }
            if (added > 0) {
                msg.append("*" + added + "* added ");
            }
            if (removed > 0) {
                msg.append("*" + removed + "* removed");
            }
        }
        return msg.toString();
    }

}
