package com.tcg.ci.webhook.service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tcg.ci.webhook.cache.TelegramUpdateCache;
import com.tcg.ci.webhook.config.TelegramConfig;
import com.tcg.ci.webhook.utils.OkHttpUtils;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.MultipartBody.Part;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Slf4j
@Service
public class TelegramWebhookService implements ITelegramWebhookService {

    @Autowired
    private OkHttpUtils okHttpUtils;

    @Autowired
    private TelegramUpdateCache updateCache;

    @Autowired
    private TelegramConfig telegramConfig;

    @Override
    public boolean setWebhookUrl() {
        String url = telegramConfig.getBotsApiUrl() + telegramConfig.getBotsApiToken() + "/" + telegramConfig.getBotsApiSetWebhookMethod();
        log.info("setWebhookUrl, url={}", url);

        File certificateFile = null;
        if (StringUtils.isNotBlank(telegramConfig.getWebhookCertificateFile())) {
            try {
                URL certificateFileUrl = TelegramWebhookService.class.getResource("/" + telegramConfig.getWebhookCertificateFile());
                certificateFile = new File(certificateFileUrl.toURI());
            } catch (URISyntaxException e) {
                log.error(e.getMessage(), e);
            }
        }

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addPart(Part.createFormData("url", telegramConfig.getWebhookUrl()));

        if (certificateFile != null) {
            builder = builder.addFormDataPart("certificate", "ngrok.pem",
                    RequestBody.create(certificateFile, MediaType.parse("multipart/form-data")));
        }

        MultipartBody requestBody = builder.build();

        Request request = new Request.Builder().url(url).post(requestBody).build();

        try (Response response = okHttpUtils.setCertificateFile(telegramConfig.getHttpsCertificateFile()).httpClient().newCall(request)
                .execute()) {
            log.info("set webhookurl response:{}", JSON.toJSONString(response));
            if (response != null && response.isSuccessful()) {
                return true;
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return false;
    }

    @Override
    public JSONObject getWebhookInfo() {
        String url = telegramConfig.getBotsApiUrl() + telegramConfig.getBotsApiToken() + "/"
                + telegramConfig.getBotsApiGetWebhookInfoMethod();
        log.info("getWebhookInfo, url={}", url);

        Request request = new Request.Builder().url(url).get().build();

        try (Response response = okHttpUtils.httpClient().newCall(request).execute()) {
            log.info("get webhookurl response:{}", JSON.toJSONString(response));
            if (response != null && response.isSuccessful()) {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    String content = responseBody.string();
                    log.info("response content={}", content);
                    return JSON.parseObject(content);
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public void onUpdate(JSONObject update) {
        if (update == null || update.get("message") == null) {
            log.error("update or message is null");
            return;
        }

        JSONObject message = update.getJSONObject("message");
        JSONObject chat = message.getJSONObject("chat");
        if (chat == null || chat.getLong("id") == null) {
            log.error("chat is null");
            return;
        }

        JSONObject from = message.getJSONObject("from");
        if (from == null || from.getLong("id") == null) {
            log.error("from is null");
            return;
        }

        String text = message.getString("text");
        if (StringUtils.isNotBlank(text) && !"/start".equals(text)) {
            log.error("ignore msg={}", text);
            return;
        }

        Long fromId = from.getLong("id");
        Long chatId = chat.getLong("id");

        String s = null;

        JSONObject updateFromCache = updateCache.getUpdateFromCache();
        JSONObject updateChatCache = updateCache.getUpdateChatCache();
        if (updateFromCache.keySet().contains(String.valueOf(fromId)) && MapUtils.isNotEmpty(updateChatCache)) {
            updateChatCache.forEach((k, v) -> {
                JSONArray charIds = Optional.fromNullable(updateChatCache.getJSONArray(k))
                        .or(new JSONArray(Lists.newCopyOnWriteArrayList()));
                if (!charIds.contains(chatId)) {
                    charIds.add(chatId);
                    log.info("add chatId to cache, updateChatCache={}", JSON.toJSONString(updateChatCache));
                }
                updateChatCache.put(k, charIds);
            });
            s = updateFromCache.getString(String.valueOf(fromId));
        } else {
            s = RandomStringUtils.random(10, true, true);
            updateFromCache.put(String.valueOf(fromId), s);
            updateChatCache.put(s, new JSONArray(new JSONArray(Lists.newCopyOnWriteArrayList(Sets.newHashSet(chatId)))));
        }
        updateCache.sync();

        if (StringUtils.isBlank(s)) {
            log.error("s is null");
            return;
        }

        String msg = telegramConfig.getWebhookUpdateUrl() + "/" + s;

        try {
            JSONObject params = new JSONObject();
            params.put("text", "git webhook url: " + msg);
            params.put("chat_id", chatId);

            RequestBody requestBody = RequestBody.create(params.toJSONString().getBytes("utf-8"), MediaType.get("application/json"));

            String url = telegramConfig.getBotsApiUrl() + telegramConfig.getBotsApiToken() + "/"
                    + telegramConfig.getBotsApiSendMessageMethod();
            log.info("onUpdate send git webhook url message, url={}", url);
            Request request = new Request.Builder().url(url).post(requestBody).build();

            @Cleanup
            Response response = okHttpUtils.httpClient().newCall(request).execute();
            log.info("update response:{}", JSON.toJSONString(response));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

}
