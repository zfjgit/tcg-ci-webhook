package com.tcg.ci.webhook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.tcg.ci.webhook.service.ITelegramWebhookService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("webhook")
public class WebhookController {

    @Autowired
    private ITelegramWebhookService service;

    @RequestMapping("test")
    public ResponseEntity<?> test() {
        return ResponseEntity.ok("ok");
    }

    @RequestMapping("setwebhook")
    public ResponseEntity<?> setWebhook() {
        boolean r = service.setWebhookUrl();
        log.info("set webhookurl result={}", r);
        return ResponseEntity.ok("ok");
    }

    @RequestMapping("url")
    public ResponseEntity<?> url(@RequestBody JSONObject update) {
        log.info("update={}", update);
        service.onUpdate(update);
        return ResponseEntity.ok(update);
    }

    @RequestMapping("getwebhook")
    public ResponseEntity<?> getWebhook() {
        JSONObject webhookInfo = service.getWebhookInfo();
        log.info("webhookInfo={}", webhookInfo);
        return ResponseEntity.ok(webhookInfo);
    }
}
