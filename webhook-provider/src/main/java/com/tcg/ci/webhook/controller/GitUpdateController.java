package com.tcg.ci.webhook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.tcg.ci.webhook.service.IGitUpdateService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("webhook/gitupdate")
public class GitUpdateController {

    @Autowired
    private IGitUpdateService service;

    @RequestMapping("/{s}")
    public ResponseEntity<?> update(@RequestBody(required = false) JSONObject update,
            @PathVariable(name = "s", required = false) String s) {
        log.info("update={}, s={}", update, s);
        service.onUpdate(update, s);
        return ResponseEntity.ok("ok");
    }
}
