package com.tcg.ci.webhook.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ControllerAdvice
@AutoConfigureAfter(TomcatHttpsConfig.class)
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @ExceptionHandler
    public ResponseEntity<?> exceptionHandler(Throwable e) {
        log.error(e.getMessage(), e);

        return ResponseEntity.ok().body(e.getMessage());
    }

}
