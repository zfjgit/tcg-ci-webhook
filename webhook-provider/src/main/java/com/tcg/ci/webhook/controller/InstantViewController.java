package com.tcg.ci.webhook.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("instant-view")
public class InstantViewController {

    @ResponseBody
    @RequestMapping("test")
    public ResponseEntity<?> test() {
        log.info("test");
        return ResponseEntity.ok("ok");
    }

    @RequestMapping("compare-template")
    public ModelAndView compare(String compareUrl, String commits, String beforeAfter, String changes) {
        log.info("compare....");
        ModelAndView mav = new ModelAndView("compare");
        mav.addObject("compareUrl", compareUrl);
        mav.addObject("commits", commits);
        mav.addObject("beforeAfter", beforeAfter);
        mav.addObject("changes", changes);
        return mav;
    }
}
