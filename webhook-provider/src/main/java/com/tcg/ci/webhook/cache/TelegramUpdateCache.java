package com.tcg.ci.webhook.cache;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URI;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;

import lombok.Cleanup;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Component
public class TelegramUpdateCache implements Serializable, ITelegramCache {
    /**
    *
    */
    private static final long serialVersionUID = -789015489012981590L;

    private transient static final ReadWriteLock lock = new ReentrantReadWriteLock();

    private JSONObject updateChatCache;

    private JSONObject updateFromCache;

    @Override
    @PostConstruct
    public void init() {
        try {
            if (updateChatCache != null) {
                return;
            }

            lock.writeLock().lock();

            if (updateChatCache != null) {
                return;
            }

            updateChatCache = doInit("/update-chat.cache");

            updateFromCache = doInit("/update-from.cache");
        } finally {
            lock.writeLock().unlock();
        }
    }

    private static JSONObject doInit(String filePath) {
        JSONObject cache = new JSONObject(Maps.newConcurrentMap());
        try {
            InputStream is = TelegramUpdateCache.class.getResourceAsStream(filePath);
            @Cleanup
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));

            String content = null;
            while ((content = br.readLine()) != null) {
                if (StringUtils.isBlank(content)) {
                    continue;
                }

                log.info("init cache, filePath={}, content={}", filePath, content);
                cache.putAll(JSON.parseObject(content));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return cache;
    }

    @Override
    public synchronized void sync() {
        doSync("/update-chat.cache", updateChatCache.toJSONString());
        doSync("/update-from.cache", updateFromCache.toJSONString());
    }

    private static void doSync(String filePath, String content) {
        try {
            log.info("sync cache, filePath={}, content={}", filePath, content);

            URI uri = TelegramUpdateCache.class.getResource(filePath).toURI();
            @Cleanup
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(uri)));
            bw.write(content);
            bw.flush();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public JSONObject getUpdateChatCache() {
        if (updateChatCache == null) {
            return new JSONObject(Maps.newConcurrentMap());
        }
        return updateChatCache;
    }

    public JSONObject getUpdateFromCache() {
        if (updateFromCache == null) {
            return new JSONObject(Maps.newConcurrentMap());
        }
        return updateFromCache;
    }
}
