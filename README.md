# Telegram and Gitlab Integration
- [springboot支持https](https://juejin.im/post/5b44b4fef265da0f767530f7)
- [导入cer证书到jdk](https://stackoverflow.com/questions/21076179/pkix-path-building-failed-and-unable-to-find-valid-certification-path-to-requ)
- [ssllabs工具](https://www.ssllabs.com/ssltest/analyze.html?d=707c4e9c.ngrok.io)
- [openssl工具](https://stackoverflow.com/questions/50625283/how-to-install-openssl-in-windows-10)
- [证书格式及区别](https://blog.51cto.com/wushank/1915795)
- [telegram文档-setwebhook](https://core.telegram.org/bots/api#setwebhook)
- [telegram文档-证书](https://core.telegram.org/bots/self-signed)
- [telegram文档-instantview](https://instantview.telegram.org/)
- [okhttp文档](https://square.github.io/okhttp/https/)
- [ngrok文档](https://dashboard.ngrok.com/get-started)
- [thymeleaf](https://www.thymeleaf.org/)


# Spring Boot Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/maven-plugin/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#configuration-metadata-annotation-processor)
* [Spring cache abstraction](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-caching)
* [Spring Data Redis (Access+Driver)](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-redis)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#production-ready)
* [Cloud Bootstrap](https://spring.io/projects/spring-cloud-commons)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Caching Data with Spring](https://spring.io/guides/gs/caching/)
* [Messaging with Redis](https://spring.io/guides/gs/messaging-redis/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)