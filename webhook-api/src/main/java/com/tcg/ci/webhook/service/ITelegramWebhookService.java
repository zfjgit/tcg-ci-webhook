package com.tcg.ci.webhook.service;

import com.alibaba.fastjson.JSONObject;

public interface ITelegramWebhookService {

    boolean setWebhookUrl();

    JSONObject getWebhookInfo();

    void onUpdate(JSONObject update);

}
