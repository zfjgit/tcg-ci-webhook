package com.tcg.ci.webhook.service;

import com.alibaba.fastjson.JSONObject;

public interface IGitUpdateService {

    void onUpdate(JSONObject update, String s);
}
