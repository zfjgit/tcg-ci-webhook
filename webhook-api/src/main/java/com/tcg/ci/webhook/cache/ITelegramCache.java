package com.tcg.ci.webhook.cache;

public interface ITelegramCache {

    void init();

    void sync();
}
