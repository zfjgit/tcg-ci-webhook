package com.tcg.ci.webhook.data.git;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Commit {
    @JsonProperty("id")
    private String id;

    @JsonProperty("message")
    private String message;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("url")
    private String url;

    @JsonProperty("added")
    private List<String> added = null;

    @JsonProperty("modified")
    private List<String> modified = null;

    @JsonProperty("removed")
    private List<String> removed = null;

    @JsonProperty("author")
    private Author author;
}