package com.tcg.ci.webhook.utils;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;

@Slf4j
@Component
public class OkHttpUtils {

//    private String certificateFile;

    private SSLSocketFactory sslSocketFactory;

    private X509TrustManager x509TrustManager;

    public SSLSocketFactory getSslSocketFactory() {
        if (sslSocketFactory != null) {
            return sslSocketFactory;
        }

        try {
            SSLContext sslContext = null;

//            @Cleanup
//            InputStream certificates = null;
//            if (StringUtils.isNotBlank(certificateFile)) {
//                certificates = OkHttpUtils.class.getResourceAsStream("/" + certificateFile);
//            }
//
//            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
//            Certificate ca = certificateFactory.generateCertificate(certificates);

            // Create a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            sslContext = SSLContext.getInstance("TLS");
//            sslContext.init(null, tmf.getTrustManagers(), null);
            sslContext.init(null, new TrustManager[] { getX509TrustManager() }, null);

            sslSocketFactory = sslContext != null ? sslContext.getSocketFactory() : null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return sslSocketFactory;
    }

    public X509TrustManager getX509TrustManager() {
        if (x509TrustManager != null) {
            return x509TrustManager;
        }

        x509TrustManager = new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        };

        return x509TrustManager;
    }

    public OkHttpClient httpClient() {
        return new OkHttpClient.Builder().sslSocketFactory(getSslSocketFactory(), getX509TrustManager())
                .readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).build();
    }

    public OkHttpUtils setCertificateFile(String certificateFile) {
//        this.certificateFile = certificateFile;
        return this;
    }

    public static void main(String[] args) {
        try {
            URL resource = OkHttpUtils.class.getResource("/1");
            File file = new File(resource.toURI());
            log.info("file={}", file);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e);
        }
    }
}
